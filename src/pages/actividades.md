---
title: 🗒️ Actividades 

---


## Primer encuentro.

- Crear un listado de tareas que pasen por los estados *abiertas* y *cerradas* en el marco de un *Proyecto*
  1. En [GitLab](https://gitlab.com "Enlace a GitLab"), crearse una cuenta o loguearse si tienen una. [🧶 Turorial Registro](/docs/gitlab/registro/ "Tutorial Registro")
  2. Crear un *Proyecto* en blanco. Solo vamos a usar los *Issues* cada Issue será una tarea. [🧶 Tutorial Proyectos](/docs/gitlab/proyectos/ "Tutorial Proyectos")
  3. Elegir una actividad  y segmentarla en taras. Abrirlas, describirlas y marcarlas como terminadas al finalizar. [🧶Tutorial Issues ](/docs/gitlab/issues/ "Tutorial Issues")
