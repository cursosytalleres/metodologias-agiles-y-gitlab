---
title: Presentaciones
---

# Presentaciones de curso Oct-Nov 2021

- <a href="https://alvarmaciel.gitlab.io/desarrollos-agiles/slides/clase-1.html" target="_blank">Primer Encuentro</a>
- <a href="https://alvarmaciel.gitlab.io/desarrollos-agiles/slides/scrum.html" target="_blank">Segundo Encuentro </a>
