## Organización del curso
Este curso está orientado a personas que no tienen experiencia en desarrollo del software, pero quieren incorporar metodologías y herramientas de gestión propias de este campo para sus desarrollos profesionales.

Consta de 2 encuentros semanales por cada tema. Los encuentros son virtuales en formato taller, de 90 minutos.  Una serie de actividades entre encuentros, material de referencia y un espacio de consulta asincrónico.

En el último encuentro se evalúa el curso y la propuesta de trabajo final

### Temas:

- Metodologías ágiles para la gestión de proyectos:
  - Scrum como metodo para el desarrollo de proyectos
- Escritura colaborativa asincrónica de documentación
  - Git como herramienta de producción de documentos personales y colaborativos

### Objetivos de aprendizaje de cada tema
#### Metodologías ágiles para la gestión de proyectos
Comprender la estructura de la metodología Ágil Scrum

- Backlog
- Sprint

Planificar y ejecutar un sprint usando herramientas de GitLab.

- Issues
- Labels
- Milestones
- Rqueriments
- Boards

#### Escritura colaborativa asincrónica de documentación

Comprender el funcionamiento de GIT
- Comprender las operaciones básicas para el mantenimiento de un repositorio personal:  
  - clone
  - stage
  - stash
  - commit
  - Push
 - Pull

- Experimentar la escritura colaborativa en un repositorio común
  - workflows
  - Branching
  - Merge Request
  - Conflictos

### Evaluación
La evaluación del curso se hará contra entrega de un proyecto simple. Este proyecto se adaptará a los temas trabajados, si solo se aborda &ldquo;Metodologías ágiles para la gestión de proyectos&rdquo; el entregable puede ser en cualquier tipo de documento de texto (online y offline). Si se toma el curso entero, el entregable deberá ser un repositorio en [Gitlab](https://gitlab.com))

Los participantes tendrán que organizar un \*recetario. Lo suficientemente amplio como para que se puedan realizar al menos, las comidas de una semana (7 días). 
El recetario tiene que incluir:

-   Índice
-   Tabla de proporciones
-   Recetas por categoría
-   Anexo con las fuentes de las recetas
-   Anexo con plan de alimentación semanal

En el repositorio donde se organizará el trabajo tiene que evidenciarse

-   El listado de tareas y sus prioridades
-   Un board en el que se pueda ver el estado de las tareas
-   al menos 3 sprints representados en milestones
-   EL archivo README contendrá un link a el/los documentos que usaron.

### Requerimientos
-   Conexión a internet
-   Computadora (Puede usarse un celular para el encuentro sincrónico)
-   Uso básico de plataformas web:
    -   creación de usuarios,
    -   gestión de cuentas
### Dinámica de clases

-   no más de 1/2 hora de exposición del tema
-   1/2 o más para realización de actividades
-   Espacio de Q&A

### Actividades

-   Tema: &ldquo;Metodologías Ágiles para la gestión de proyectos&rdquo;
    -   Construcción de aviones en la primer clase
    -   Primer sprint del recetario en la segunda clase



### Materiales útiles

- Apuntes de clase
- [Blog de Paulina cocina](https://www.paulinacocina.net/)
- [Avioncitos de papel](https://avioncitosdepapel.com/)
- Drive para videos de vuelo
