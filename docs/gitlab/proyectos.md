---
sidebar_position: 3
title: Crear un proyecto nuevo 🚀
---


import { VideoYouTube } from '../../src/components/VideoYouTube';

## Paso a paso

Para crear un proyecto nuevo:

1. Ir a crear proyectos
  
  ![Creación de proyectos](/img/gitlabProjects1.png "Creación de proyectos")
  
2. Crear proyecto en blanco 
  
  ![Creación de proyectos en blanco](/img/gitlabProjects2.png "Creación de proyectos en blanco")

3. Asignarle un nombre

  ![Creación de proyectos Nombre](/img/gitlabProjects3.png "Creación de proyectos Nombre")
  

4. Definr la visbilidad como "Públic" Por ahora lo vamos a hacer así. Son pruebas

  ![Creación de proyectos visibilidad](/img/gitlabProjects4.png "Creación de proyectos Visibilidad")

5. Click en botón `Create project`

![Creación de proyectos Listo](/img/gitlabProjects5.png "Creación de proyectos Listo")

## Video tutorial


<VideoYouTube
  id="Bbbm2XJb1eg"
  titulo="Crear un Proyecto en Gitlab"
  descripcion="Cómo crear un proyecto en Gitlab"
/>
