---
sidebar_position: 4
title: Crear una tarea 🚀
---


import { VideoYouTube } from '../../src/components/VideoYouTube';

## Paso a paso

Para crear una *tarea* nueva, usamos el elemento **Issues** de Gitlab:

1. Ir a `Issues` -> `List`

  ![Issues](/img/gitlabIssues1.png "Issues")
  
3. Click en botón `New issue`

  ![New Issue](/img/gitlabIssues2.png "New Issues")
  
4. Asignarle un título y si lo desean  un contenido. 👀 Se escribe en formato [Markdown](/docs/markdown/sintaxis/ "Sintaxis de Markdown") 👀

## Vista de edición de la tarea

![Edicion en Markdown](/img/gitlabIssues3.png "Edicion en Markdown")

## Vista Final

![Vista Final](/img/gitlabIssues4.png "Vista Final")

## Video tutorial

<VideoYouTube
  id="sMj4Jn6X2wk"
  titulo="Crear Issues en GitLab"
  descripcion="Cómo crear un Issue en Gitlab"
/>
