---
sidebar_position: 1
title: Sprints

---

> Este artículo está escrito en base a la lectura y notas tomadas de [Scrum: El arte de hacer el doble de trabajo en la mitad de tiempo](https://www.amazon.com/Scrum-trabajo-tiempo-definici%C3%B3n-Spanish-ebook/dp/B01EIQAALK) y el sito [Scrum Guides](https://scrumguides.org/)

## Scrum origenes y definiciones

El término "ágil" dentro del marco de la industria del desarrollo de software se remonta al año 2001, en una reunión en la que diecisiete líderes de desarrollo de software escribieron lo que hoy se conoce como el Manifiesto por el Desarrollo Ágil de Software o [Agile Manifesto](https://agilemanifesto.org/iso/es/manifesto.html), en ese documento se pone en valor:

>- **Individuos e interacciones** sobre procesos y herramientas
>- **Software funcionando** sobre documentación extensiva
>- **Colaboración con el cliente** sobre negociación contractual
>- **Respuesta ante el cambio** sobre seguir un plan
>
> Esto es, aunque valoramos los elementos de la derecha, valoramos más los de la izquierda.

**Scrum** es un marco de trabajo liviano que ayuda a las personas, equipos y organizaciones a generar valor 
a través de soluciones adaptativas para problemas complejos. Creado por  [Jeff Sutherland](https://www.scruminc.com/scrum-blog/) y [Ken Schwaber](https://kenschwaber.wordpress.com/) que pone los valores del Manifiesto Ágil en práctica. 
Según sus autores, no es ninguna metodología. Sino más bien un conjunto de ideas rectoras sobre la coordinación de un proyecto con una serie de prácticas basadas en esas ideas.

Lo cierto es que este conjunto de prácticas e ideas renovaron la industria del desarrollo de software desplazando lentamente las planificaciones en cascada. Generando un marco de prácticas para la ejecución de proyectos de cualquier tipo y a diversas escalas. Desde la creación de un programa, la refacción de una casa o la escritura de un libro. 
Este marco se adapta a las necesidades de proyectos que requieran equipos de trabajo... o incluso proyectos individuales, como el de este curso.

![gestion de escritura del curso](/img/gestionDeCursoAgile.png)


Scrum se organiza a través de  una serie de **Valores**, **Eventos** y **Artefactos**, estos conceptos organizan los elementos del marco de trabajo.

Un vistazo general de Scrum sería el siguiente[^1]

![](/img/agileCicle.png)

En este texto vamos a revisar el corazón de Scrum: Los Sprints

## Sprints
> Scrum se basa en una idea simple: cada vez que se ejecuta un proyecto, ¿por qué no revisar con regularidad para ver si lo que se está haciendo sigue la dirección correcta y es lo que la gente quiere? ¿Por qué no revisar si se puede hacer mejor y más rápido y qué lo impide?


![](/img/agileSprint.jpg)

En este marco, Scrum opera estableciendo metas secuenciales para cumplir en un período de tiempo fijo. Se llaman **Sprints** a estos ciclos fijos. Los Sprints **eventos** de duración fija de un mes o menos para crear consistencia. Un nuevo Sprint comienza 
inmediatamente después de la conclusión del Sprint anterior. 

Se basa en el principio de **Inspección y ajuste**: Cada tanto hacer una pausa, revisar lo que se hizo y ver si se debe seguir haciendo o si se puede hacer mejor.

Al comienzo de cada Sprint hay una reunión o **Planeamiento del Sprint** en la que se evalúan las tareas de  hechas y se planean las tareas a llevar a cabo durante este ciclo. El equipo decide cuánto cree poder hacer en las semanas siguientes y en base a esto selecciona las tareas a realizar.

Esta selección, se toma de tareas que están predefinidas en el **Backlog**. El Backlog es un listado con todas las tareas que se estiman realizar para llevar a cabo el proyecto y que fueron negociadas con el cliente.

Un elemento crucial de cada sprint, sin embargo, es que una vez que el equipo se compromete con lo que hará, el número de tareas se congela. Nadie fuera del equipo puede añadir una más.

Por otro lado, se recomiendan reuniones periódicas para hacer un seguimiento del Sprint, estas son las **Paradas Diarias**

### Planeamiento del Sprint

Reunión que inicia el Sprint estableciendo el trabajo que se realizará durante este ciclo. El equipo crea este plan resultante mediante trabajo colaborativo. 


Aborda los siguientes temas: 
- ¿Por qué es valioso este Sprint o por qué es importante?
- ¿Qué se puede hacer en este Sprint?
- ¿Cómo se realizará el trabajo elegido?

Importante: No hay asignación de tareas desde arriba; el equipo es autónomo: se asigna a sí mismo sus tareas.

### Paradas diarias

Estas son reuniones periódicas dentro de un Sprint, breves, simples, si pueden estar de a pie mejor y no pueden durar más de 15 minutos. Sirven para mejorar los proceso dentro de las tareas designadas en el Sprint y apuntan a responder tres cosas:

1. ¿Qué se hizo ayer para ayudar al equipo a terminar este sprint?
2. ¿Qué haremos hoy para ayudar al equipo a terminar este sprint?
3. ¿Qué obstruye el avance del equipo?

Reglas:
- La reunión es siempre a la misma hora
- No puede exceder los 15 minutos
- Todos deben participar activamente


### ideas detrás del Sprint

- La gestión del Sprint está inspirado en el ciclo Planea, Haz, Revisa, Actúa (Plan, Do, Check, Act, PDCA). Este ciclo puede aplicarse a la producción de prácticamente cualquier cosa, sea un auto, un videojuego, un avión de papel o mapas conceptuales.[^1]

- “actuar” significa cambiar la manera de trabajar con base en los resultados reales y las aportaciones reales del entorno. 

- No suponer; planear, hacer, revisar, actuar. Planear qué se hará. Hacerlo. Revisa si se logro lo que se buscaba. Actúar en consecuencia y cambiar la manera de hacer las cosas. Repetir esto en ciclos regulares para alcanzar la mejora continua.

##### Imágenes
- Imagen de Scrum [Metodología Scrum en proyecto digitales](https://blog.ida.cl/estrategia-digital/metodologia-scrum-en-proyectos-digitales/)
- Imagen de Sprint de [¿Qué es un SPRINT en Scrum?](https://ittude.com.ar/b/scrum/que-es-un-sprint/)


[^1] ver: [W. Edwards Deming](https://es.wikipedia.org/wiki/William_Edwards_Deming)
