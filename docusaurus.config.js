// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Desarrollos Ágiles y Gitlab',
  tagline: '❤️‍🔥 Scrum e Issues ❤️‍🔥',
  url: 'https://alvarmaciel.gitlab.io/desarrollos-agiles',
  baseUrl: '/desarrollos-agiles/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'Alvar Maciel', // Usually your GitHub org/user name.
  projectName: 'Desarrollos Ágiles', // Usually your repo name.
  trailingSlash: true,
   i18n: {
     defaultLocale: 'es',
     locales: ['es'],
   },

  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://gitlab.com/alvarmaciel/desarrollos-agiles/-/edit/main/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/main/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Desarrollos Ágiles',
        logo: {
          alt: 'Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Materiales',
          },
          {to: '/actividades', label: 'Actividades', position: 'left' },
          {to: '/presentaciones', label: 'Presentaciones', position: 'left'},
          {
            href: 'https://gitlab.com/alvarmaciel/desarrollos-agiles',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Notas teóricas',
                to: '/docs/intro',
              },
              {
                label: 'Presentaciones',
                to: '/presentaciones',
              },
            ],
          },
          // {
          //   title: 'Community',
          //   items: [
          //     {
          //       label: 'Stack Overflow',
          //       href: 'https://stackoverflow.com/questions/tagged/docusaurus',
          //     },
          //     {
          //       label: 'Discord',
          //       href: 'https://discordapp.com/invite/docusaurus',
          //     },
          //     {
          //       label: 'Twitter',
          //       href: 'https://twitter.com/docusaurus',
          //     },
          //   ],
          // },
          {
            title: 'Mas del autor',
            items: [
              {
                label: 'Cyberiadas',
                href: 'https://alvarmaciel.gitlab.io/cyberiada/',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/alvarmaciel/desarrollos-agiles/',
              },
            ],
          },
        ],
        copyright: `<p style="margin-bottom: 0"><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/deed.es">Licencia Creative Commons Atribución-CompartirIgual 4.Internacional 0</a>. <br/>${new Date().getFullYear()} Alvar Maciel`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
